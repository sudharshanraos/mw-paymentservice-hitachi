/** 
 * COPYRIGHT: Comviva Technologies Pvt. Ltd.
 * This software is the sole property of Comviva
 * and is protected by copyright law and international
 * treaty provisions. Unauthorized reproduction or
 * redistribution of this program, or any portion of
 * it may result in severe civil and criminal penalties
 * and will be prosecuted to the maximum extent possible
 * under the law. Comviva reserves all rights not
 * expressly granted. You may not reverse engineer, decompile,
 * or disassemble the software, except and only to the
 * extent that such activity is expressly permitted
 * by applicable law notwithstanding this limitation.
 * THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 * YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
 * AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
 * ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
 * USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
package com.tarang.middleware.paymentservice.utils;

import java.util.Collection;
import java.util.Locale;

/**
 * This final class used for common utilities methods.
 * 
 * @author sudharshans
 */
public final class CommonUtils {

    /**
     * Instantiates a new common utils.
     */
    private CommonUtils() {
    }

    /**
     * Validate the null check.
     *
     * @param collection
     *            - collection
     * @return boolean
     */
    public static boolean isNullorEmpty(Collection<?> collection) {
        return (null == collection || collection.isEmpty());
    }

    /**
     * Validate the null check for Object super class.
     *
     * @param str
     *            - input
     * @return boolean
     */
    public static boolean isNullorEmpty(Object str) {
        return (null == str || "".equals(str) || "null".equals(str) || "undefined".equals(str));
    }

    /**
     * To upper case.
     *
     * @param str
     *            the str
     * @return the string
     */
    public static String toUpperCase(String str) {
        return !isNullorEmpty(str) ? str.toUpperCase(Locale.ENGLISH) : null;
    }

    /**
     * Validate the null check and get string value.
     *
     * @param str
     *            - input
     * @return String
     */
    public static String getString(Object str) {
        if (isNullorEmpty(str)) {
            return "";
        }
        return String.valueOf(str);
    }

    /**
     * Validate the Numeric or not.
     *
     * @param str
     *            - input
     * @return boolean
     */
    public static boolean isNumeric(String str) {
        return (!isNullorEmpty(str) && str.chars().allMatch(Character::isDigit));
    }

    /**
     * Validate the Alphabets or not.
     *
     * @param str
     *            - input
     * @return boolean
     */
    public static boolean isAlphabets(String str) {
        return (!isNullorEmpty(str) && str.chars().allMatch(Character::isLetter));
    }

    /**
     * Validate the Alpha Numeric or not.
     *
     * @param str
     *            - input
     * @return boolean
     */
    public static boolean isAlphaNumeric(String str) {
        return (!isNullorEmpty(str) && (str.chars().allMatch(Character::isLetterOrDigit)));
    }

    /**
     * Validate the null and zero.
     *
     * @param input
     *            -- input
     * @return boolean
     */
    public static boolean isNullorZero(Long input) {
        return (null == input || 0 >= input);
    }

    /**
     * Validate the null and zero.
     *
     * @param input
     *            -- input
     * @return boolean
     */
    public static boolean isNullorZero(Double input) {
        return (null == input || 0 >= input);
    }

    /**
     * To lower case.
     *
     * @param str
     *            the str
     * @return the string
     */
    public static String toLowerCase(String str) {
        return !isNullorEmpty(str) ? str.toLowerCase(Locale.ENGLISH) : null;
    }
}
