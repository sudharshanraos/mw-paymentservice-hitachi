/** 
 * COPYRIGHT: Comviva Technologies Pvt. Ltd.
 * This software is the sole property of Comviva
 * and is protected by copyright law and international
 * treaty provisions. Unauthorized reproduction or
 * redistribution of this program, or any portion of
 * it may result in severe civil and criminal penalties
 * and will be prosecuted to the maximum extent possible
 * under the law. Comviva reserves all rights not
 * expressly granted. You may not reverse engineer, decompile,
 * or disassemble the software, except and only to the
 * extent that such activity is expressly permitted
 * by applicable law notwithstanding this limitation.
 * THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 * YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
 * AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
 * ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
 * USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
package com.tarang.middleware.paymentservice.utils;

import java.math.BigInteger;
import java.util.Locale;

import org.jpos.iso.ISOBinaryField;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;

import com.tarang.middleware.paymentservice.enums.TransactionTypes;

/**
 * This final class used for iso common utilities methods.
 * 
 * @author sudharshans
 */
public final class ISOCommonUtils {

    /**
     * Instantiates a new iso common utils.
     */
    private ISOCommonUtils() {
    }

    /**
     * Adds the zeros if required.
     *
     * @param str
     *            the str
     * @return the string
     */
    public static String addZerosIfRequired(String str) {
        int length = ISOUtil.hex2byte(str).length * 2;
        for (int i = 0; i < 8; i++) {
            if (length % 8 != 0) {
                str = str + "00";
                length = ISOUtil.hex2byte(str).length;
            } else {
                return str;
            }
        }
        return "";
    }

    /**
     * Gets the hexa string.
     *
     * @param str
     *            the str
     * @return the hexa string
     */
    public static String getHexaString(String str) {
        return String.format(Locale.US, "%x", new BigInteger(1, str.getBytes()));
    }

    /**
     * Gets the hexa byte.
     *
     * @param str
     *            the str
     * @return the hexa byte
     */
    public static byte[] getHexaByte(String str) {
        return ISOUtil.hex2byte(String.format(Locale.US, "%x", new BigInteger(1, str.getBytes())));
    }

    /**
     * Gets the string from hex.
     *
     * @param hex
     *            the hex
     * @return the string from hex
     */
    public static String getStringFromHex(String hex) {
        if (hex != null && hex.length() != 0) {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < hex.length(); i += 2) {
                str.append((char) Integer.parseInt(hex.substring(i, i + 2), 16));
            }
            return str.toString();
        } else
            return "";
    }

    /**
     * Map the track 2 data.
     *
     * @param track2
     *            the track 2
     * @throws ISOException
     */
    public static void mapTrack2Data(ISOMsg isoMsg, int position, String track2) throws ISOException {
        if (!CommonUtils.isNullorEmpty(track2)) {
            if (track2.endsWith("F")) {
                track2 = track2.substring(0, track2.length() - 1);
            } else if (37 <= track2.trim().length()) {
                track2 = (track2.substring(0, 37));
            }
            isoMsg.set(new ISOField(position, track2));
        }
    }

    /**
     * Map Mti process code.
     *
     * @param isoMsg
     *            the iso msg
     * @param transactionType
     *            the transaction type
     * @throws ISOException
     *             the ISO exception
     */
    public static void mapMtiProcessCode(ISOMsg isoMsg, TransactionTypes transactionType) throws ISOException {
        isoMsg.setMTI(transactionType.getReqMTI());
        isoMsg.set(new ISOField(3, transactionType.getProcessingCode()));
    }

    /**
     * Map ISO field.
     *
     * @param isoMsg
     *            the iso msg
     * @param position
     *            the position
     * @param value
     *            the value
     * @throws ISOException
     *             the ISO exception
     */
    public static void mapISOField(ISOMsg isoMsg, int position, String value) throws ISOException {
        if (!CommonUtils.isNullorEmpty(value)) {
            isoMsg.set(new ISOField(position, value));
        }
    }

    /**
     * Map ISO binary field.
     *
     * @param isoMsg
     *            the iso msg
     * @param position
     *            the position
     * @param value
     *            the value
     * @throws ISOException
     *             the ISO exception
     */
    public static void mapISOBinaryField(ISOMsg isoMsg, int position, String value) throws ISOException {
        if (!CommonUtils.isNullorEmpty(value)) {
            isoMsg.set(new ISOBinaryField(position, ISOUtil.hex2byte(value)));
        }
    }
}
