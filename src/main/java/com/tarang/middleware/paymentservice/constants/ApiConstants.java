package com.tarang.middleware.paymentservice.constants;

/**
 * The Class ApiConstants.
 * 
 * @author sudharshan.s
 */
public final class ApiConstants {

    /** The Constant PARENT. */
    public static final String PARENT = "/hitachi";

    /** The Constant NETWORK_MANAGEMENT. */
    public static final String NETWORK_MANAGEMENT = "Network Management Services";

    /** The Constant LOGON. */
    public static final String LOGON = "/logon";

    /** The Constant ECHO_TRANSACTION. */
    public static final String ECHO_TRANSACTION = "/echo";

    /** The Constant AUTHORIZATION_MANAGEMENT. */
    public static final String AUTHORIZATION_MANAGEMENT = "Authorization Management Services";

    /** The Constant AUTH. */
    public static final String AUTH = "/auth";

    /** The Constant FINANCIAL_MANAGEMENT. */
    public static final String FINANCIAL_MANAGEMENT = "Financial Management Services";

    /** The Constant FINANCIAL. */
    public static final String SALE = "/sale";

    /** The Constant FINANCIAL_ADVICE. */
    public static final String FINANCIAL_ADVICE = "/sale/advice";

    /** The Constant REVERSAL_MANAGEMENT. */
    public static final String REVERSAL_MANAGEMENT = "Reversal Management Services";

    /** The Constant REVERSAL. */
    public static final String REVERSAL = "/reverse";

    /** The Constant REVERSAL_ADVICE. */
    public static final String REVERSAL_ADVICE = "/reverse/advice";

    /** The Constant SYSTEM_CONFIG_MANAGEMENT. */
    public static final String SYSTEM_CONFIG_MANAGEMENT = "System Configuration Management";

    /** The Constant SYSTEM_PARENT. */
    public static final String SYSTEM_PARENT = "/system";
    
    /** The Constant SYSTEM_GET_ENCRYPT_DATA. */
    public static final String SYSTEM_GET_ENCRYPT_DATA = "/encrypt";

    /** The Constant SYSTEM_GET_DECRYPT_DATA. */
    public static final String SYSTEM_GET_DECRYPT_DATA = "/decrypt";

    /**
     * Instantiates a new api constants.
     */
    private ApiConstants() {
        // private constructor
    }

}
