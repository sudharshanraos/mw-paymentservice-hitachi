package com.tarang.middleware.paymentservice.constants;

/**
 * The Class ApiConstants.
 * 
 * @author sudharshan.s
 */
public final class Constants {

    /**
     * Instantiates a new constants.
     */
    private Constants() {
        // private constructor
    }

}
