package com.tarang.middleware.paymentservice.controller;

import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.paymentservice.constants.ApiConstants;
import com.tarang.middleware.paymentservice.dto.TransactionRequest;
import com.tarang.middleware.paymentservice.dto.TransactionResponse;
import com.tarang.middleware.paymentservice.enums.ResponseCodes;
import com.tarang.middleware.paymentservice.enums.TransactionTypes;
import com.tarang.middleware.paymentservice.service.io.PaymentHostService;
import com.tarang.middleware.paymentservice.service.iso.ISOMessageFactory;
import com.tarang.middleware.paymentservice.service.iso.ISOMessageSimulator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * This controller provides network management services for hitachi.
 *
 * @author sudharshan.s
 */
@RestController
@RequestMapping(ApiConstants.PARENT)
@Api(value = ApiConstants.NETWORK_MANAGEMENT, tags = {
        ApiConstants.NETWORK_MANAGEMENT
})
public class NetworkManagementController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(NetworkManagementController.class);

    /** The message factory. */
    @Autowired
    private ISOMessageFactory messageFactory;

    /** The payment host service. */
    @Autowired
    private PaymentHostService paymentHostService;

    /** The iso message simulator. */
    @Autowired
    private ISOMessageSimulator isoMessageSimulator;

    /**
     * Echo.
     *
     * @param txnRequest
     *            the txn request
     * @return the transaction response
     */
    @PostMapping(value = ApiConstants.ECHO_TRANSACTION, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Process the echo transaction")
    @ResponseStatus(HttpStatus.OK)
    public TransactionResponse echo(@RequestBody TransactionRequest txnRequest) {
        LOGGER.info("Process the echo transaction");
        TransactionResponse response = new TransactionResponse();
        ISOMsg isoResponse = null;
        try {
            ISOMsg isoRequest = messageFactory.createISOMessage(txnRequest.getMessage(), TransactionTypes.ECHO_TRANSACTION);
            isoResponse = isoMessageSimulator.echoResponse(isoRequest);
            // isoResponse = paymentHostService.send(isoRequest,
            // txnRequest.getHost(), txnRequest.getPort());
            response.setRespsonseCode(isoResponse.getString(39));
            response.setResponseMessage(ResponseCodes.getResponseCodes(isoResponse.getString(39)).getMessage());
        } catch (Exception e) {
            LOGGER.error("ERROR: Process the echo transaction  - {}", e);
        }
        return response;
    }

    /**
     * Logon.
     *
     * @param txnRequest
     *            the txn request
     * @return the transaction response
     */
    @PostMapping(value = ApiConstants.LOGON, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Process the logon transaction")
    @ResponseStatus(HttpStatus.OK)
    public TransactionResponse logon(@RequestBody TransactionRequest txnRequest) {
        LOGGER.info("Process the logon transaction");
        TransactionResponse response = new TransactionResponse();
        ISOMsg isoResponse = null;
        try {
            ISOMsg isoRequest = messageFactory.createISOMessage(txnRequest.getMessage(), TransactionTypes.LOGON);
            isoResponse = isoMessageSimulator.logonResponse(isoRequest);
            // isoResponse = paymentHostService.send(isoRequest,
            // txnRequest.getHost(), txnRequest.getPort());
            response.setRespsonseCode(isoResponse.getString(39));
            response.setResponseMessage(ResponseCodes.getResponseCodes(isoResponse.getString(39)).getMessage());
        } catch (Exception e) {
            LOGGER.error("ERROR: Process the logon transaction  - {}", e);
        }
        return response;
    }

}
