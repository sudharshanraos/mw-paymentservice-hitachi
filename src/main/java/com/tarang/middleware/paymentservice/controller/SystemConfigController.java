package com.tarang.middleware.paymentservice.controller;

import org.jasypt.util.text.AES256TextEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.paymentservice.constants.ApiConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * This controller provides inter-communication system configuration apis.
 * 
 * @author sudharshan.s
 */
@RestController
@RequestMapping(ApiConstants.SYSTEM_PARENT)
@Api(value = ApiConstants.SYSTEM_CONFIG_MANAGEMENT, tags = {
        ApiConstants.SYSTEM_CONFIG_MANAGEMENT
})
public class SystemConfigController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SystemConfigController.class);

    /**
     * Gets the encrypt data.
     *
     * @param plainText
     *            the plain text
     * @param pkey
     *            the pkey
     * @return the encrypt data
     */
    @GetMapping(value = ApiConstants.SYSTEM_GET_ENCRYPT_DATA, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get File Configurations")
    @ResponseStatus(HttpStatus.OK)
    public String getEncryptData(@ApiParam(required = true) @RequestParam("plainText") String plainText,
            @ApiParam(required = true) @RequestParam("pkey") String pkey) {
        LOGGER.info("Get Encrypt Data");
        AES256TextEncryptor encryptor = new AES256TextEncryptor();
        encryptor.setPassword(pkey);
        return encryptor.encrypt(plainText);
    }

    /**
     * Gets the decrypt data.
     *
     * @param encText
     *            the enc text
     * @param pkey
     *            the pkey
     * @return the decrypt data
     */
    @GetMapping(value = ApiConstants.SYSTEM_GET_DECRYPT_DATA, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Decrypt Data")
    @ResponseStatus(HttpStatus.OK)
    public String getDecryptData(@ApiParam(required = true) @RequestParam("encText") String encText,
            @ApiParam(required = true) @RequestParam("pkey") String pkey) {
        LOGGER.info("Get Decrypt Data");
        AES256TextEncryptor encryptor = new AES256TextEncryptor();
        encryptor.setPassword(pkey);
        return encryptor.decrypt(encText);
    }

}
