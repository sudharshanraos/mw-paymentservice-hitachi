package com.tarang.middleware.paymentservice.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class TransactionResponse.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@NoArgsConstructor
public class TransactionResponse extends ISOBaseResponse implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

}
