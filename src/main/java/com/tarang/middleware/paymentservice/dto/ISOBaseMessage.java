package com.tarang.middleware.paymentservice.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class ISOBaseMessage.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ISOBaseMessage implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The mti 0. */
    private String mti0;

    /** The pan 2. */
    private String pan2;

    /** The processing code 3. */
    private String processingCode3;

    /** The amt transaction 4. */
    private String amtTransaction4;

    /** The amt settlement 5. */
    private String amtSettlement5;

    /** The amt cardholderbilling 6. */
    private String amtCardholderbilling6;

    /** The transmission date time 7. */
    private String transmissionDateTime7;

    /** The amt cardholderbillingfee 8. */
    private String amtCardholderbillingfee8;

    /** The conversation rate sattlement 9. */
    private String conversationRateSattlement9;

    /** The conversation rate cardholderbilling 10. */
    private String conversationRateCardholderbilling10;

    /** The  stan 11. */
    private String stan11;

    /** The time local transaction 12. */
    private String timeLocalTransaction12;

    /** The date local transaction 13. */
    private String dateLocalTransaction13;

    /** The date expiration 14. */
    private String dateExpiration14;

    /** The date sattlement 15. */
    private String dateSattlement15;

    /** The date conversion 16. */
    private String dateConversion16;

    /** The date capture 17. */
    private String dateCapture17;

    /** The merchant type 18. */
    private String merchantType18;

    /** The country code acquiringinstitution 19. */
    private String countryCodeAcquiringinstitution19;

    /** The country code primaryaccountno 20. */
    private String countryCodePrimaryaccountno20;

    /** The country code forwardinginstitution 21. */
    private String countryCodeForwardinginstitution21;

    /** The pos entrymode 22. */
    private String posEntrymode22;

    /** The card sequence number 23. */
    private String cardSequenceNumber23;

    /** The functioncode 24. */
    private String functioncode24;

    /** The message reason code 25. */
    private String messageReasonCode25;

    /** The card acceptor business code 26. */
    private String cardAcceptorBusinessCode26;

    /** The autid res length 27. */
    private String autidResLength27;

    /** The reconsilation date 28. */
    private String reconsilationDate28;

    /** The amt sattlement fee 29. */
    private String amtSattlementFee29;

    /** The amt tran processing fee 30. */
    private String amtTranProcessingFee30;

    /** The amt sattle processing fee 31. */
    private String amtSattleProcessingFee31;

    /** The accuring insitute id code 32. */
    private String accuringInsituteIdCode32;

    /** The forward insitute id code 33. */
    private String forwardInsituteIdCode33;

    /** The primary account number extended 34. */
    private String primaryAccountNumberExtended34;

    /** The track 2 data 35. */
    private String track2Data35;

    /** The track 3 data 36. */
    private String track3Data36;

    /** The retri ref no 37. */
    private String retriRefNo37;

    /** The auth id res code 38. */
    private String authIdResCode38;

    /** The response code 39. */
    private String responseCode39;

    /** The service restric code 40. */
    private String serviceRestricCode40;

    /** The card acceptor tem id 41. */
    private String cardAcceptorTemId41;

    /** The card acceptor id code 42. */
    private String cardAcceptorIdCode42;

    /** The card acceptor name location 43. */
    private String cardAcceptorNameLocation43;

    /** The additional res data 44. */
    private String additionalResData44;

    /** The track one data 45. */
    private String trackOneData45;

    /** The amounts fees 46. */
    private String amountsFees46;

    /** The additional data national 47. */
    private String additionalDataNational47;

    /** The additional data private 48. */
    private String additionalDataPrivate48;

    /** The curr code transaction 49. */
    private String currCodeTransaction49;

    /** The curr code statle ment 50. */
    private String currCodeStatleMent50;

    /** The currency code cardholderbilling 51. */
    private String currencyCodeCardholderbilling51;

    /** The pin data 52. */
    private String pinData52;

    /** The sec related cont info 53. */
    private String secRelatedContInfo53;

    /** The addl amt 54. */
    private String addlAmt54;

    /** The icc card system related data 55. */
    private String iccCardSystemRelatedData55;

    /** The msg reason code 56. */
    private String msgReasonCode56;

    /** The auth life code 57. */
    private String authLifeCode57;

    /** The auth agent id code 58. */
    private String authAgentIdCode58;

    /** The echo data 59. */
    private String echoData59;

    /** The reserved data 60. */
    private String reservedData60;

    /** The reserved data 61. */
    private String reservedData61;

    /** The reserved data 62. */
    private String reservedData62;

    /** The reserved data 63. */
    private String reservedData63;

    /** The message authentication code field 64. */
    private String messageAuthenticationCodeField64;

}
