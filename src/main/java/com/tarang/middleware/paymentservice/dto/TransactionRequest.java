package com.tarang.middleware.paymentservice.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class TransactionRequest.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@NoArgsConstructor
public class TransactionRequest implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The host. */
    private String host;
    
    /** The port. */
    private String port;
    
    /** The tpdu header. */
    private String tpduHeader;
    
    /** The packager path. */
    private String packagerPath;
    
    /** The message. */
    private ISOBaseMessage message;

}
