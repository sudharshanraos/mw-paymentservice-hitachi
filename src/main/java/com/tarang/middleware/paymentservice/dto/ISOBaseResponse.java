package com.tarang.middleware.paymentservice.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class contains the general response to be returned across API's in this
 * service.
 *
 * @author sudharshan.s
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ISOBaseResponse implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The response code. */
    private String respsonseCode;

    /** The response message. */
    private String responseMessage;

}
