package com.tarang.middleware.paymentservice.service.iso;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tarang.middleware.paymentservice.enums.ResponseCodes;
import com.tarang.middleware.paymentservice.enums.TransactionTypes;
import com.tarang.middleware.paymentservice.utils.ISOCommonUtils;

/**
 * The Class ISOMessageSimulator.
 * 
 * @author sudharshan.s
 */
@Service
public class ISOMessageSimulator {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ISOMessageSimulator.class);

    /**
     * Echo response.
     *
     * @param request
     *            the request
     * @return the ISO msg
     */
    public ISOMsg echoResponse(ISOMsg request) {
        ISOMsg response = new ISOMsg();
        try {
            response.setMTI(TransactionTypes.ECHO_TRANSACTION.getResMTI());
            ISOCommonUtils.mapISOField(response, 3, TransactionTypes.ECHO_TRANSACTION.getProcessingCode());
            ISOCommonUtils.mapISOField(response, 11, request.getString(11));
            ISOCommonUtils.mapISOField(response, 12, getDateTime(new Date(), "hhmmss"));
            ISOCommonUtils.mapISOField(response, 13, getDateTime(new Date(), "MMyy"));
            ISOCommonUtils.mapISOField(response, 39, ResponseCodes.RES00.getCode());
            ISOCommonUtils.mapISOField(response, 63, request.getString(63));
            response.dump(new PrintStream(System.err), "");
        } catch (Exception e) {
            LOGGER.error("ERROR: generate the echo transaction-{}", e);
        }
        return response;
    }

    /**
     * Logon response.
     *
     * @param request
     *            the request
     * @return the ISO msg
     */
    public ISOMsg logonResponse(ISOMsg request) {
        ISOMsg response = new ISOMsg();
        try {
            response.setMTI(TransactionTypes.LOGON.getResMTI());
            ISOCommonUtils.mapISOField(response, 3, TransactionTypes.LOGON.getProcessingCode());
            ISOCommonUtils.mapISOField(response, 11, request.getString(11));
            ISOCommonUtils.mapISOField(response, 12, getDateTime(new Date(), "hhmmss"));
            ISOCommonUtils.mapISOField(response, 13, getDateTime(new Date(), "MMyy"));
            ISOCommonUtils.mapISOField(response, 39, ResponseCodes.RES00.getCode());
            ISOCommonUtils.mapISOField(response, 41, request.getString(41));
            ISOCommonUtils.mapISOField(response, 42, request.getString(42));
            ISOCommonUtils.mapISOBinaryField(response, 48, "00184B50D7E5AC895576706CEB69D43D99E17B8F");
            response.dump(new PrintStream(System.err), "");
        } catch (Exception e) {
            LOGGER.error("ERROR: generate the logon transaction-{}", e);
        }
        return response;
    }

    /**
     * Sale response.
     *
     * @param request
     *            the request
     * @return the ISO msg
     */
    public ISOMsg saleResponse(ISOMsg request) {
        ISOMsg response = new ISOMsg();
        try {
            response.setMTI(TransactionTypes.SALE.getResMTI());
            ISOCommonUtils.mapISOField(response, 3, TransactionTypes.SALE.getProcessingCode());
            ISOCommonUtils.mapISOField(response, 4, request.getString(4));
            ISOCommonUtils.mapISOField(response, 11, request.getString(11));
            ISOCommonUtils.mapISOField(response, 12, getDateTime(new Date(), "hhmmss"));
            ISOCommonUtils.mapISOField(response, 13, getDateTime(new Date(), "MMyy"));
            ISOCommonUtils.mapISOField(response, 37, "000000" + request.getString(11));
            ISOCommonUtils.mapISOField(response, 38, "0000CB");
            ISOCommonUtils.mapISOField(response, 39, ResponseCodes.RES00.getCode());
            ISOCommonUtils.mapISOField(response, 41, request.getString(41));
            response.dump(new PrintStream(System.err), "");
        } catch (Exception e) {
            LOGGER.error("ERROR: generate the sale transaction-{}", e);
        }
        return response;
    }

    /**
     * Gets the date time.
     *
     * @param date
     *            the date
     * @param dateFormat
     *            the date format
     * @return the date time
     */
    public static String getDateTime(Date date, String dateFormat) {
        SimpleDateFormat df = new SimpleDateFormat(dateFormat);
        return df.format(date);
    }

}
