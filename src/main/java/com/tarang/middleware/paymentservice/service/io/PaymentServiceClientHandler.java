package com.tarang.middleware.paymentservice.service.io;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * The Class PaymentServiceClientHandler.
 * 
 * @author sudharshan.s
 */
@Sharable
@Service
public class PaymentServiceClientHandler extends SimpleChannelInboundHandler<ISOMsg> {

    /** The Constant log. */
    private static final Logger log = LoggerFactory.getLogger(PaymentServiceClientHandler.class);

    /** The message. */
    private ISOMsg message;

    /** The answer. */
    final BlockingQueue<ISOMsg> answer = new LinkedBlockingQueue<ISOMsg>();

    /**
     * Channel active.
     *
     * @param ctx
     *            the ctx
     * @throws Exception
     *             the exception
     */
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
    }

    /**
     * Channel read 0.
     *
     * @param ctx
     *            the ctx
     * @param msg
     *            the msg
     * @throws Exception
     *             the exception
     */
    protected void channelRead0(ChannelHandlerContext ctx, ISOMsg msg) throws Exception {
        answer.offer(msg);
    }

    /**
     * Exception caught.
     *
     * @param ctx
     *            the ctx
     * @param cause
     *            the cause
     * @throws Exception
     *             the exception
     */
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        log.error("Exception Caught {} ", cause.toString());
        ctx.close();
    }

    /**
     * Gets the ISO msg.
     *
     * @return the ISO msg
     */
    public ISOMsg getISOMsg() {
        try {
            message = answer.take();
        } catch (InterruptedException e) {
            log.error("Exception caught in getISOMsg method  DciClientHandler clas : {}", e);
        }
        return message;
    }

}
