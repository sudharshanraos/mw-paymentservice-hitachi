package com.tarang.middleware.paymentservice.service.io;

import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarang.middleware.paymentservice.service.iso.ISOMessageFactory;
import com.tarang.middleware.paymentservice.utils.ByteConversionUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * The Class PaymentServiceEncoder.
 */
@Sharable
@Service
public class MessageEncoder extends MessageToByteEncoder<ISOMsg> {

    /** The Constant log. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageEncoder.class);

    /** The message factory. */
    @Autowired
    private ISOMessageFactory messageFactory;

    /**
     * Encode.
     *
     * @param ctx
     *            the ctx
     * @param msg
     *            the msg
     * @param out
     *            the out
     * @throws Exception
     *             the exception
     */
    @Override
    public void encode(ChannelHandlerContext ctx, ISOMsg isoMsg, ByteBuf out) throws Exception {
        try {
            byte[] isoBuffer = messageFactory.packISOMsg(isoMsg);
            byte[] isoLength = ByteConversionUtils.intToByteArray(isoBuffer.length);
            byte[] finalIsoData = ISOUtil.concat(isoLength, isoBuffer);
            LOGGER.info("HEX >>> {}", ByteConversionUtils.byteArrayToHexString(finalIsoData, finalIsoData.length, false));
            out.writeBytes(finalIsoData);
        } catch (Exception e) {
            LOGGER.error("Unable to encode data : {}", e);
            throw new IllegalStateException("unable to encode data, " + e.getMessage());
        }
    }

}
