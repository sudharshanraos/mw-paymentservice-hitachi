package com.tarang.middleware.paymentservice.service.io;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;

import javax.net.ssl.KeyManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;

/**
 * The Class PaymentServiceChannelInitializer.
 * 
 * @author sudharshan.s
 */
@Service
public class PaymentServiceChannelInitializer extends ChannelInitializer<SocketChannel> {

    /** The Constant log. */
    private static final Logger log = LoggerFactory.getLogger(PaymentServiceChannelInitializer.class);

    /** The message encoder. */
    @Autowired
    private MessageEncoder messageEncoder;

    /** The message decoder. */
    @Autowired
    private MessageDecoder messageDecoder;

    /** The client handler. */
    @Autowired
    private PaymentServiceClientHandler clientHandler;

    /** The ssl enabled. */
    @Value("${paymentservice.sslEnabled}")
    private String sslEnabled;

    /** The sslkeystore path. */
    @Value("${paymentservice.sslKeystorePath}")
    private String sslkeystorePath;

    /** The sslkeystore password. */
    @Value("${paymentservice.sslKeystorePassword}")
    private String sslkeystorePassword;

    /**
     * Inits the channel.
     *
     * @param channel
     *            the channel
     * @throws Exception
     *             the exception
     */
    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        log.debug("Initialize Channel {}", channel);
        ChannelPipeline pipeline = channel.pipeline();
        if (Boolean.parseBoolean(sslEnabled)) {
            log.debug("ssl Enabled");
            SslContextBuilder ctxBuilder = SslContextBuilder.forClient().keyManager(getKeyManager());
            SslContext sslCtx = ctxBuilder.build();
            pipeline.addLast("ssl", sslCtx.newHandler(channel.alloc()));
        } else {
            log.debug("ssl not Enabled");
        }
        pipeline.addLast("encoder", messageEncoder);
        pipeline.addLast("decoder", messageDecoder);
        pipeline.addLast("handler", clientHandler);
    }

    /**
     * Gets the key manager.
     *
     * @return the key manager
     * @throws Exception
     *             the exception
     */
    private KeyManagerFactory getKeyManager() throws Exception {
        KeyStore ks = null;
        FileInputStream fis = null;
        try {
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
            fis = new java.io.FileInputStream(sslkeystorePath);
            ks.load(fis, sslkeystorePassword.toCharArray());
        } catch (KeyStoreException e) {
            log.error("KeyStoreException caught in getKeyManager() method in  DciChannelInitializer class : {}", e);
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
        KeyManagerFactory kmf;
        kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(ks, sslkeystorePassword.toCharArray());
        return kmf;
    }

}
