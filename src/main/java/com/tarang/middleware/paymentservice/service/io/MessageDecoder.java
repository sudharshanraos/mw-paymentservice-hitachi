package com.tarang.middleware.paymentservice.service.io;

import java.util.List;

import org.apache.tomcat.util.buf.HexUtils;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarang.middleware.paymentservice.service.iso.ISOMessageFactory;
import com.tarang.middleware.paymentservice.utils.ByteConversionUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.bytes.ByteArrayDecoder;

/**
 * The Class PaymentServiceDecoder.
 * 
 * @author sudharshan.s
 */
@Sharable
@Service
public class MessageDecoder extends ByteArrayDecoder {

    /** The Constant log. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageDecoder.class);

    /** The message factory. */
    @Autowired
    private ISOMessageFactory messageFactory;

    /**
     * Decode.
     *
     * @param ctx
     *            the ctx
     * @param in
     *            the in
     * @param out
     *            the out
     * @throws Exception
     *             the exception
     */
    @Override
    public void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        try {
            if (in.isReadable()) {
                byte[] responseMessage = new byte[in.readableBytes()];
                in.getBytes(0, responseMessage);
                LOGGER.info("Get Response from host : {} ", HexUtils.toHexString(responseMessage));
                // Get hex request
                String hexReq = ByteConversionUtils.byteArrayToHexString(responseMessage, responseMessage.length, false);
                // Get length of request
                int reqLen = Integer.parseInt(hexReq.substring(0, 4), 16);
                byte[] afterBuffer = ISOUtil.hex2byte(hexReq.substring(4, hexReq.length()).trim().getBytes(), 0, reqLen);
                ISOMsg isoMessage = new ISOMsg();
                if (reqLen == afterBuffer.length) {
                    isoMessage = messageFactory.unpackISOFormat(afterBuffer);
                } else {
                    LOGGER.info("Receive the packet length not match...");
                }
                in.clear();
                out.add(isoMessage);
            }
        } catch (Exception e) {
            LOGGER.error("Unexpected exception while decoding  : {}", e);
            throw new IllegalArgumentException("unexpected exception", e);
        }
    }

}
