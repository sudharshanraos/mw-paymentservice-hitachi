package com.tarang.middleware.paymentservice.service.iso;

import org.jpos.iso.*;

/**
 * The Class HitachiPackager.
 * 
 * @author sudharshan.s
 */
public class HitachiPackager extends ISOBasePackager {

    /** The fld. */
    protected ISOFieldPackager fld[] = {
            /* 000 */new IFA_NUMERIC(4, "Message Type Indicator"), //
            /* 001 */new IFA_BITMAP(16, "Secondary Bitmap"), //
            /* 002 */new IFA_LLBINARY(19, "Primary Account number"), //
            /* 003 */new IFA_NUMERIC(6, "Processing Code"), //
            /* 004 */new IFA_NUMERIC(12, "Amount, Transaction"), //
            /* 005 */new IFA_NUMERIC(12, "Amount, Settlement"), //
            /* 006 */new IFA_NUMERIC(12, "Amount, Cardholder billing"), //
            /* 007 */new IFA_NUMERIC(10, "Date and time, transmission"), //
            /* 008 */new IFA_NUMERIC(12, "Amount, Cardholder billing fee"), //
            
            /* 009 */new IFA_NUMERIC(8, "Conversion rate, Settlement"), //
            /* 010 */new IFA_NUMERIC(8, "Conversion rate, Cardholder billing"), //
            /* 011 */new IFA_NUMERIC(6, "Systems trace audit number"), //
            /* 012 */new IFA_NUMERIC(6, "Time, Local Transaction"), //
            /* 013 */new IFA_NUMERIC(4, "Date, Local Transaction"), //
            /* 014 */new IFA_NUMERIC(4, "Date, Expiration"), //
            /* 015 */new IFA_NUMERIC(4, "Date, Settlement"), //
            /* 016 */new IFA_NUMERIC(4, "Date, Conversion"), //
            
            /* 017 */new IFA_NUMERIC(4, "Date, Capture"), //
            /* 018 */new IFA_NUMERIC(4, "Merchant type"), //
            /* 019 */new IFA_NUMERIC(3, "Country code, Acquiring institution"), //
            /* 020 */new IFA_NUMERIC(3, "Country code, Primary account number"), //
            /* 021 */new IFA_NUMERIC(3, "Country code, Forwarding institution"), //
            /* 022 */new IFA_NUMERIC(3, "Point of service data code"), //
            /* 023 */new IFA_NUMERIC(3, "Card sequence number"), //
            /* 024 */new IFA_NUMERIC(3, "Function code"), //
            
            /* 025 */new IFA_NUMERIC(2, "Message Reason Code"), //
            /* 026 */new IFA_NUMERIC(4, "Card Acceptor Business Code"), //
            /* 027 */new IFA_NUMERIC(1, "Authorization ID Response Length"), //
            /* 028 */new IFA_NUMERIC(6, "Reconciliation Date"), //
            /* 029 */new IFA_NUMERIC(1 + 8, "Amount, Settlement Fee"), //
            /* 030 */new IFA_NUMERIC(12, "Original Amount"), //
            /* 031 */new IFA_NUMERIC(1 + 8, "Amount, Settle Processing Fee"), //
            /* 032 */new IFA_LLNUM(11, "Acquirer institution identification code"), //
            
            /* 033 */new IFA_LLNUM(11, "Forwarding institution identification code"), //
            /* 034 */new IFA_LLCHAR(19, "Primary account number, extended"), //
            /* 035 */new IFA_LLBINARY(37, "Track 2 data"), //
            /* 036 */new IFA_LLLCHAR(104, "Track 3 data"), //
            /* 037 */new IF_CHAR(12, "Retrieval reference number"), //
            /* 038 */new IF_CHAR(6, "Approval Code"), //
            /* 039 */new IF_CHAR(2, " Action Code"), //
            /* 040 */new IFA_NUMERIC(3, "Service Restriction Code"), //

            /* 041 */new IF_CHAR(8, "Card Acceptor Terminal ID"), //
            /* 042 */new IF_CHAR(15, "Card Acceptor ID Code"), //
            /* 043 */new IF_CHAR(40, "Card Acceptor Name Location"), //
            /* 044 */new IFA_LLCHAR(10, "Additional Response Data"), //
            /* 045 */new IFA_LLCHAR(76, "Track 1 data"), //
            /* 046 */new IFA_LLNUM(12, "Amounts, Fees"), //
            /* 047 */new IFA_LLLCHAR(999, " Card Scheme Sponsor ID & Additional data"), //
            /* 048 */new IFA_LLLBINARY(999, "Additional data - private"), //
            
            /* 049 */new IFA_NUMERIC(3, "Currency code, Transaction"), //
            /* 050 */new IFA_NUMERIC(3, "Currency code, Reconciliation"), //
            /* 051 */new IFA_NUMERIC(3, "Currency code, Cardholder billing"), //
            /* 052 */new IFB_BINARY(8, "Personal identification number [PIN] data"), //
            /* 053 */new IFA_LLBINARY(48, "Security related control information"), //
            /* 054 */new IFA_LLLCHAR(120, "Amounts, additional"), //
            /* 055 */new IFA_LLLBINARY(255, "ICC related data"), //
            /* 056 */new IFA_LLNUM(6, " Original Data Elements "), //
            
            /* 057 */new IFA_LLLNUM(3, "Authorization Life-cycle Code"), //
            /* 058 */new IFA_LLLNUM(11, "Authorizing agent institution Id Code"), //
            /* 059 */new IFA_LLLCHAR(999, " Transport Data "), //
            /* 060 */new IFA_LLLCHAR(999, "Reserved for national use"), //
            /* 061 */new IFA_LLLCHAR(999, "Reserved for national use"), //
            /* 062 */new IFA_LLLCHAR(999, "Terminal Status- Private"), //
            /* 063 */new IFA_LLLCHAR(999, "Reserved for private use"), //
            /* 064 */new IFB_BINARY(8, "Message authentication code"), //
    };

    /**
     * Instantiates a new hitachi packager.
     */
    public HitachiPackager() {
        super();
        setFieldPackager(fld);
    }
}
