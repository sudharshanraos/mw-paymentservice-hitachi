package com.tarang.middleware.paymentservice.service.io;

import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * The Class PaymentHostService.
 * 
 * @author sudharshan.s
 */
@Service
public class PaymentHostService {

    /** The Constant log. */
    private static final Logger log = LoggerFactory.getLogger(PaymentHostService.class);

    /** The ps channel initializer. */
    @Autowired
    private PaymentServiceChannelInitializer psChannelInitializer;

    /** The bootstrap. */
    private Bootstrap bootstrap;

    /** The group. */
    private EventLoopGroup group;

    /**
     * Send.
     *
     * @param request
     *            the request
     * @return the host response
     * @throws Exception
     *             the exception
     */
    public ISOMsg send(ISOMsg request, String host, String port) throws Exception {
        ISOMsg response = null;
        try {
            bootstrap = new Bootstrap();
            group = new NioEventLoopGroup();
            bootstrap.group(group).channel(NioSocketChannel.class).handler(psChannelInitializer);
            
            ChannelFuture channelFuture = bootstrap.connect(host, Integer.valueOf(port)).sync();
            Channel channel = channelFuture.channel();
            channel.writeAndFlush(request);
            log.info("Is request sent to host: {}", channel.isWritable());
            if (channel.isWritable()) {
                log.info("Host address: {}", channel.remoteAddress());
                Channel channelRead = channel.read();
                PaymentServiceClientHandler handler = (PaymentServiceClientHandler) channelRead.pipeline().last();
                response = handler.getISOMsg();
                log.info("Response from host: {}", response);
            } else {
                log.error("Operation timeout: Can't write.");
            }
        } catch (Exception e) {
            log.warn("Unknown network error  : {}", e);
        } finally {
            group.shutdownGracefully();
        }
        return response;
    }

}
