package com.tarang.middleware.paymentservice.service.iso;

import java.io.PrintStream;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tarang.middleware.paymentservice.dto.ISOBaseMessage;
import com.tarang.middleware.paymentservice.enums.TransactionTypes;
import com.tarang.middleware.paymentservice.utils.ByteConversionUtils;
import com.tarang.middleware.paymentservice.utils.ISOCommonUtils;

/**
 * The Class ISORequest.
 * 
 * @author sudharshan.s
 */
@Service
public class ISOMessageFactory {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ISOMessageFactory.class);

    /**
     * Creates a new ISOMessage object.
     *
     * @param reqObj
     *            the req obj
     * @param transactionType
     *            the transaction type
     * @return the ISO msg
     */
    public ISOMsg createISOMessage(ISOBaseMessage reqObj, TransactionTypes transactionType) {
        // Create the ISO message
        ISOMsg isoMsg = new ISOMsg();
        try {
            ISOCommonUtils.mapMtiProcessCode(isoMsg, transactionType);
            ISOCommonUtils.mapISOBinaryField(isoMsg, 2, reqObj.getPan2());
            ISOCommonUtils.mapISOField(isoMsg, 4, reqObj.getAmtTransaction4());
            ISOCommonUtils.mapISOField(isoMsg, 7, reqObj.getTransmissionDateTime7());
            ISOCommonUtils.mapISOField(isoMsg, 11, ISOUtil.padleft(reqObj.getStan11(), 6, '0'));
            ISOCommonUtils.mapISOField(isoMsg, 12, reqObj.getTimeLocalTransaction12());
            ISOCommonUtils.mapISOField(isoMsg, 13, reqObj.getDateLocalTransaction13());
            ISOCommonUtils.mapISOField(isoMsg, 14, reqObj.getDateExpiration14());
            ISOCommonUtils.mapISOField(isoMsg, 22, reqObj.getPosEntrymode22());
            ISOCommonUtils.mapISOField(isoMsg, 24, reqObj.getFunctioncode24());
            ISOCommonUtils.mapISOField(isoMsg, 25, reqObj.getMessageReasonCode25());
            ISOCommonUtils.mapTrack2Data(isoMsg, 35, reqObj.getTrack2Data35());
            ISOCommonUtils.mapISOField(isoMsg, 37, reqObj.getRetriRefNo37());
            ISOCommonUtils.mapISOField(isoMsg, 38, reqObj.getAuthIdResCode38());
            ISOCommonUtils.mapISOField(isoMsg, 39, reqObj.getResponseCode39());
            ISOCommonUtils.mapISOField(isoMsg, 41, reqObj.getCardAcceptorTemId41());
            ISOCommonUtils.mapISOField(isoMsg, 42, reqObj.getCardAcceptorIdCode42());
            ISOCommonUtils.mapISOField(isoMsg, 44, reqObj.getAdditionalResData44());
            ISOCommonUtils.mapISOField(isoMsg, 45, reqObj.getTrackOneData45());
            ISOCommonUtils.mapISOBinaryField(isoMsg, 48, reqObj.getAdditionalDataPrivate48());
            ISOCommonUtils.mapISOBinaryField(isoMsg, 52, reqObj.getPinData52());
            ISOCommonUtils.mapISOField(isoMsg, 54, reqObj.getAddlAmt54());
            ISOCommonUtils.mapISOBinaryField(isoMsg, 55, reqObj.getIccCardSystemRelatedData55());
            ISOCommonUtils.mapISOField(isoMsg, 56, reqObj.getMsgReasonCode56());
            ISOCommonUtils.mapISOField(isoMsg, 60, reqObj.getReservedData60());
            ISOCommonUtils.mapISOField(isoMsg, 61, reqObj.getEchoData59());
            ISOCommonUtils.mapISOField(isoMsg, 62, reqObj.getReservedData62());
            ISOCommonUtils.mapISOField(isoMsg, 63, reqObj.getReservedData63());
            ISOCommonUtils.mapISOBinaryField(isoMsg, 64, reqObj.getMessageAuthenticationCodeField64());
            isoMsg.dump(new PrintStream(System.err), "");
        } catch (Exception e) {
            LOGGER.error("ERROR: create jpos iso message with json iso base message - {}", e);
        }
        return isoMsg;
    }

    /**
     * Pack ISO msg.
     *
     * @param isoMsg
     *            the iso msg
     * @return the byte[]
     */
    public byte[] packISOMsg(ISOMsg isoMsg) {
        byte[] isoRequest = new byte[1024];
        try {
            // Packing the ISO
            isoMsg.setPackager(new HitachiPackager());
            isoRequest = isoMsg.pack();
            LOGGER.info("STRING >>> {} ", ByteConversionUtils.byteArrayToString(isoRequest, isoRequest.length));
            LOGGER.info("HEX >>> {}", ByteConversionUtils.byteArrayToHexString(isoRequest, isoRequest.length, true));
            isoMsg.dump(new PrintStream(System.err), "");
        } catch (Exception e) {
            LOGGER.error("ERROR: create iso request with ISO message - {}", e);
        }
        return isoRequest;
    }

    /**
     * This method unpacks the raw data and packages it into a ISOMessage format
     * as specified by the format of the packager that ViVO Pay determines. It
     * also validates the generated ISOMessage
     *
     * @param rawBytes
     *            the raw bytes
     * @return ISOMsg
     * @throws ISOException
     *             the ISO exception
     */
    public ISOMsg unpackISOFormat(byte[] rawBytes) throws ISOException {
        ISOMsg isoMessage = new ISOMsg();
        try {
            isoMessage.setPackager(new HitachiPackager());
            LOGGER.info("STRING >>> {} ", ByteConversionUtils.byteArrayToString(rawBytes, rawBytes.length));
            LOGGER.info("HEX >>> {}", ByteConversionUtils.byteArrayToHexString(rawBytes, rawBytes.length, false));
            isoMessage.unpack(rawBytes);
            isoMessage.dump(new PrintStream(System.err), "");
        } catch (Exception e) {
            LOGGER.error("ERROR: unpacking the iso request - {}", e);
        }
        return isoMessage;
    }

}
