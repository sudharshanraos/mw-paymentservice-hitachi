package com.tarang.middleware.paymentservice.enums;

import lombok.Getter;

/**
 * ResponseCodes.
 * 
 * @author sudharshan.s
 */
@Getter
public enum ResponseCodes {

    RES00("00", "Successful/Approved"), //
    RES01("01", "Refer to Card Issuer"), //
    RES02("02", "Refer to Card Issuer"), //
    RES03("03", "Invalid Merchant"), //
    RES04("04", "Pick-up Card"), //
    RES05("05", "Do not honour"), //
    RES06("06", "Error"), //
    RES12("12", "Invalid Transaction"), //
    RES13("13", "Invalid Amount"), //
    RES14("14", "Invalid Card Number"), //
    RES15("15", "No such Issuer"), //
    RES30("30", "Format Error"), //
    RES33("33", "Expired Card, Pick-up"), //
    RES34("34", "Suspected Fraud, Pick-up"), //
    RES36("36", "Restricted Card, Pick-up"), //
    RES38("38", "PIN Tries Exceeded, Pick-up"), //
    RES39("39", "No Credit Account"), //
    RES41("41", "Lost Card, Pick-up"), //
    RES42("42", "No Universal Account"), //
    RES43("43", "Stolen Card, Pick-up"), //
    RES51("51", "Not Sufficient Funds"), //
    RES52("52", "No Check Account"), //
    RES53("53", "No Savings Account"), //
    RES54("54", "Expired Card"), //
    RES55("55", "Incorrect PIN"), //
    RES57("57", "Transaction Not Permitted to Cardholder"), //
    RES58("58", "Transaction Not Permitted on terminal"), //
    RES59("59", "Suspected Fraud"), //
    RES61("61", "Exceeds Withdrawal Limit"), //
    RES62("62", "Restricted Card"), //
    RES63("63", "Security Violation"), //
    RES75("75", "PIN Tries Exceeded"), //
    RES89("89", "TID not present on host"), //
    RES91("91", "Issuer Not Available"), //
    RES92("92", "Routing Error"), //
    RES95("95", "Reconcile Error"), //
    RES96("96", "System Malfunction"), //
    RES98("98", "Exceeds Cash Limit"), //
    RESL1("L1", "Merchant Daily Limit Exceeded"), //
    RESL2("L2", "Card Re-use Limit Exceeded"), //
    RESL3("L3", "International Acceptance not enabled"),
    ;

    /** The code. */
    private final String code;

    /** The message. */
    private final String message;

    /**
     * Instantiates a new response codes.
     *
     * @param code
     *            the code
     * @param message
     *            the message
     */
    ResponseCodes(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * Gets the response codes.
     *
     * @param code
     *            the code
     * @return the response codes
     */
    public static ResponseCodes getResponseCodes(String code) {
        for (ResponseCodes v : ResponseCodes.values()) {
            if (v.name().equalsIgnoreCase(code)) {
                return v;
            }
        }
        return RES00;
    }
}
