package com.tarang.middleware.paymentservice.enums;

import lombok.Getter;

/**
 * The Enum TransactionTypes.
 * 
 * @author sudharshan.s
 */
@Getter
public enum TransactionTypes {

    LOGON("LOGON", "0800", "0810", "920000"), //
    ECHO_TRANSACTION("ECHO_TRANSACTION", "0800", "0810", "990000"), //

    PRE_AUTH("PRE_AUTH", "0100", "0110", "000000"), //
    BHARAT_QR_GENERATION("BHARAT_QR_GENERATION", "0100", "0110", "330001"), //
    BHARAT_QR_INQUIRY("BHARAT_QR_INQUIRY", "0100", "0110", "330000"), //

    SALE("SALE", "0200", "0210", "00aa00"), //
    SALE_CASH("SALE_CASH", "0200", "0210", "09aa00"), //
    VOID_SALE("VOID_SALE", "0200", "0210", "02aa00"), //
    MONEY_LOAD("MONEY_LOAD", "0200", "0210", "82aa00"), //
    BALANCE_UPDATE("BALANCE_UPDATE", "0200", "0210", "84aa00"), //
    SERVICE_REQUEST("SERVICE_REQUEST", "0200", "0210", "83aa00"), //

    PRE_AUTH_COMPLETE("PRE_AUTHORIZATION_COMPLETION", "0220", "0230", "000000"), //
    VOID_SALE_ADVICE("VOID_SALE_ADVICE", "0220", "0230", "02aa00"), //

    REVERSAL("REVERSAL", "0400", "0410", ""), //
    REVERSAL_ADVICE("REVERSAL_ADVICE", "0420", "0430", ""),//
    ;

    /** The type. */
    private final String type;

    /** The req MTI. */
    private final String reqMTI;

    /** The res MTI. */
    private final String resMTI;

    /** The processing code. */
    private final String processingCode;

    /**
     * Instantiates a new transaction types.
     *
     * @param type
     *            the type
     * @param reqMTI
     *            the req MTI
     * @param resMTI
     *            the res MTI
     * @param processingCode
     *            the processing code
     */
    TransactionTypes(String type, String reqMTI, String resMTI, String processingCode) {
        this.type = type;
        this.reqMTI = reqMTI;
        this.resMTI = resMTI;
        this.processingCode = processingCode;
    }

    /**
     * Gets the transaction type by type.
     *
     * @param type
     *            the type
     * @return the transaction type by type
     */
    public static TransactionTypes getTransactionTypeByType(String type) {
        for (TransactionTypes v : TransactionTypes.values()) {
            if (v.name().equalsIgnoreCase(type)) {
                return v;
            }
        }
        return SALE;
    }

}
