package com.tarang.middleware.paymentservice.enums;

import lombok.Getter;

/**
 * APi Details.
 * 
 * @author sudharshan.s
 */
@Getter
public enum ApiDetails {

    /** The network management. */
    NETWORK_MANAGEMENT("Network Management Services", "Hitaci Network Management Service APIs"),

    /** The authorization management. */
    AUTHORIZATION_MANAGEMENT("Authorization Management Services", "Hitaci Authorization Management Service APIs"),

    /** The financial management. */
    FINANCIAL_MANAGEMENT("Financial Management Services", "Hitaci Financial Management Service APIs"),

    /** The reversal management. */
    REVERSAL_MANAGEMENT("Reversal Management Services", "Hitaci Reversal Management Service APIs"),

    /** The system config management. */
    SYSTEM_CONFIG_MANAGEMENT("System Configuration Management", "System Configuration Management API Services"),

    ;

    /** The key. */
    private final String key;

    /** The value. */
    private final String value;

    /**
     * A mapping between the integer code and its corresponding text to
     * facilitate lookup by code.
     *
     * @param key
     *            the key
     * @param value
     *            the value
     */

    ApiDetails(String key, String value) {
        this.value = value;
        this.key = key;
    }
}
