package com.tarang.middleware.paymentservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The Class HitachiPaymentServiceApplication.
 * 
 * @author sudharshan.s
 */
@EnableScheduling
@SpringBootApplication
public class HitachiPaymentServiceApplication {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(HitachiPaymentServiceApplication.class);

    /**
     * The main method.
     *
     * @param args
     *            the arguments
     */
    public static void main(String[] args) {
        LOGGER.info("HitachiPaymentServiceApplication Starting..");
        SpringApplication.run(HitachiPaymentServiceApplication.class, args);
    }
    
}
