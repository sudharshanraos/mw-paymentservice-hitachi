package com.tarang.middleware.paymentservice.config;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tarang.middleware.paymentservice.enums.ApiDetails;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Configuration class for Swagger API Configuration.
 *
 * @author sudharshan.s
 */
@Configuration
@EnableSwagger2
@PropertySource("classpath:swagger.properties")
public class SwaggerConfig {

    /** The description. */
    @Value("${appilication.project.description}")
    private String description;

    /** The response messages. */
    private List<ResponseMessage> responseMessages = Arrays.asList(
            new ResponseMessageBuilder().code(HttpStatus.UNAUTHORIZED.value()).message("Unauthorized").build(),
            new ResponseMessageBuilder().code(HttpStatus.BAD_REQUEST.value()).message("Bad Request").build(),
            new ResponseMessageBuilder().code(HttpStatus.NOT_FOUND.value()).message("Not Found").build());

    /**
     * This method used Product the API with base package and prefix.
     *
     * @return Docket
     */
    @Bean
    public Docket productApi() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        for (ApiDetails apiDetails : ApiDetails.values()) {
            docket.tags(new Tag(apiDetails.getKey(), apiDetails.getValue()));
        }
        return docket.select().apis(RequestHandlerSelectors.basePackage("com.tarang.middleware.paymentservice")).paths(regex("/*.*")).build()
                .apiInfo(metaData()).useDefaultResponseMessages(false).globalResponseMessage(RequestMethod.GET, responseMessages)
                .globalResponseMessage(RequestMethod.POST, responseMessages).globalResponseMessage(RequestMethod.PUT, responseMessages)
                .globalResponseMessage(RequestMethod.DELETE, responseMessages);
    }

    /**
     * Set the swagger configuration with title, description and version.
     *
     * @return ApiInfo
     */
    private ApiInfo metaData() {
        return new ApiInfoBuilder().title("Hitachi Payment Services").description(description).version("1.0.0").build();
    }

    /**
     * Ui config.
     *
     * @return the ui configuration
     */
    @Bean
    UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder().deepLinking(false).displayOperationId(false).defaultModelsExpandDepth(1).defaultModelExpandDepth(1)
                .defaultModelRendering(ModelRendering.EXAMPLE).displayRequestDuration(false).docExpansion(DocExpansion.NONE).filter(false)
                .maxDisplayedTags(null).operationsSorter(OperationsSorter.ALPHA).showExtensions(false).tagsSorter(TagsSorter.ALPHA)
                .supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS).validatorUrl(null).build();
    }

}
